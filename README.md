# music-example

It's a CLI program to count the number of words of the lyrics by an artist.

It uses these API:

https://lyricsovh.docs.apiary.io/#reference

https://musicbrainz.org/doc/Development/XML_Web_Service/Version_2


# How to:

tested and working on Python 3.6.9, 3.8

### create environment:

python3 -m venv env

. env/bin/activate

python3 -m pip install -r requirements.txt


### run (from src):

usage: 

count_artist_lyrics_words.py [-h] [-f] [-u]
                                    user_agent artist_name [artist_name ...]


positional arguments:

 - user_agent:   the name of your user agent, please check:
               https://musicbrainz.org/doc/MusicBrainz_API/Rate_Limiting

 - artist_name:  the name of the artist

optional arguments:

 - -h, --help  show this help message and exit

 - -f, --first  choose the first artist automatically

 - -u, --no-found  show on console songs not found or not readable


### test (from the base dir):

python3 -m pytest .

python3 -m pytest --cov .

# Example

(cd src)

python3 count_artist_lyrics_words.py testqwewqsdjjja.com madonna -f -u

python3 count_artist_lyrics_words.py testqwewqsdjjja.com madonna -f

python3 count_artist_lyrics_words.py testqwewqsdjjja.com madonna 

python3 count_artist_lyrics_words.py -h

# TODO

It could make sense to package it.

