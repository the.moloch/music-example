import builtins
import pytest
from io import StringIO
from count_artist_lyrics_words import FirstElementMenuSelector, LyricsOVH, MusicBrainzOrg, TerminalMenuSelector, count_words_in_text, main


@pytest.fixture
def wrong_artist():
    return "asdrenb4"


@pytest.fixture
def real_artist():
    return "madonna"


@pytest.fixture
def madonna_id():
    return '79239441-bfd5-4981-a70c-55c3f15c1287'


@pytest.fixture
def madonna_song():
    return "Future Lovers"


@pytest.fixture
def aerosmith_name():
    return "aerosmith"


@pytest.fixture
def aerosmith_link():
    return "https://musicbrainz.org/ws/2/artist?query=aerosmith&limit=100&offset=0&fmt=json"


@pytest.fixture
def aerosmith_lookup():
    return """{"created":"2021-11-26T10:19:25.458Z","count":14,"offset":0,"artists":[{"id":"3d2b98e5-556f-4451-a3ff-c50ea18d57cb","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":100,"name":"Aerosmith","sort-name":"Aerosmith","country":"US","area":{"id":"489ce91b-6658-3307-9877-795b68554c98","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United States","sort-name":"United States","life-span":{"ended":null}},"begin-area":{"id":"e331bfdf-b908-429c-a79b-710cf9c06abb","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Boston","sort-name":"Boston","life-span":{"ended":null}},"life-span":{"begin":"1970","ended":null},"aliases":[{"sort-name":"America's Greatest Rock and Roll Band","name":"America's Greatest Rock and Roll Band","locale":null,"type":null,"primary":null,"begin-date":null,"end-date":null},{"sort-name":"Bad Boys from Boston, The","name":"The Bad Boys From Boston","locale":null,"type":null,"primary":null,"begin-date":null,"end-date":null},{"sort-name":"エアロスミス","type-id":"894afba6-2816-3c24-8072-eadb66bd04bc","name":"エアロスミス","locale":"ja","type":"Artist name","primary":true,"begin-date":null,"end-date":null},{"sort-name":"Areosmith","name":"Areosmith","locale":null,"type":null,"primary":null,"begin-date":null,"end-date":null}],"tags":[{"count":5,"name":"rock"},{"count":0,"name":"heavy metal"},{"count":0,"name":"metal"},{"count":0,"name":"american"},{"count":18,"name":"hard rock"},{"count":6,"name":"blues rock"},{"count":0,"name":"album rock"},{"count":0,"name":"soft rock"},{"count":-1,"name":"pop rock"},{"count":0,"name":"arena rock"},{"count":0,"name":"blues-rock"},{"count":0,"name":"classic pop and rock"},{"count":0,"name":"pop-metal"},{"count":0,"name":"medley a b"},{"count":0,"name":"contemporary pop/rock"},{"count":0,"name":"classic rock. blues rock"},{"count":-1,"name":"+usa (massachusetts:boston)"},{"count":1,"name":"2008 universal fire victim"}]},{"id":"e7ec8c9c-de6d-405b-b3fc-dc94695fa636","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":71,"name":"Aerosmith Rocks!","sort-name":"Aerosmith Rocks!","country":"CA","area":{"id":"71bbafaa-e825-3e15-8ca9-017dcad1748b","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"Canada","sort-name":"Canada","life-span":{"ended":null}},"begin-area":{"id":"c5446ccc-24aa-4110-8130-38c97b5d83da","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Victoria","sort-name":"Victoria","life-span":{"ended":null}},"disambiguation":"Canadian Aerosmith tribute band","life-span":{"ended":null}},{"id":"53f9c590-bc0e-402e-a7e2-4249a9f46894","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":50,"name":"Draw the Line-Aerosmith Tribute","sort-name":"Draw the Line-Aerosmith Tribute","country":"US","area":{"id":"489ce91b-6658-3307-9877-795b68554c98","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United States","sort-name":"United States","life-span":{"ended":null}},"life-span":{"ended":null}},{"id":"6d7a48a5-0531-4b0c-b7f4-8f54462998cf","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":50,"name":"LAST CHILD - Aerosmith Experience","sort-name":"LAST CHILD - Aerosmith Experience","country":"US","area":{"id":"489ce91b-6658-3307-9877-795b68554c98","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United States","sort-name":"United States","life-span":{"ended":null}},"life-span":{"ended":null}},{"id":"c0ee7d2c-1543-4c51-8446-4bf2014f16a2","score":42,"name":"Aerosmithsonian","sort-name":"Aerosmithsonian","disambiguation":"Aerosmith tribute","life-span":{"ended":null}},{"id":"be797221-0117-45e3-81da-996f8d29db62","type":"Person","type-id":"b6e035f4-3ce9-331c-97df-83397230b0df","score":42,"gender-id":"36d3d30a-839d-3eda-8cb3-29be4384e4a9","name":"Joe Perry","sort-name":"Perry, Joe","gender":"male","country":"US","area":{"id":"489ce91b-6658-3307-9877-795b68554c98","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United States","sort-name":"United States","life-span":{"ended":null}},"begin-area":{"id":"555e5b30-25cf-40ba-a933-ce7acca5866d","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Lawrence","sort-name":"Lawrence","life-span":{"ended":null}},"disambiguation":"guitarist for Aerosmith","ipis":["00036488363","00036488559","00049088260"],"isnis":["0000000118748330"],"life-span":{"begin":"1950-09-10","ended":null},"aliases":[{"sort-name":"Perry, Anthony Joseph","type-id":"d4dcd0c0-b341-3612-a332-c0ce797b25cf","name":"Anthony Joseph Perry","locale":null,"type":"Legal name","primary":null,"begin-date":null,"end-date":null},{"sort-name":"Perry","name":"Perry","locale":null,"type":null,"primary":null,"begin-date":null,"end-date":null}],"tags":[{"count":1,"name":"blues rock"},{"count":1,"name":"hard rock"}]},{"id":"9fc37bbf-c2a4-4ac9-95de-255e93fc6758","type":"Person","type-id":"b6e035f4-3ce9-331c-97df-83397230b0df","score":39,"gender-id":"36d3d30a-839d-3eda-8cb3-29be4384e4a9","name":"Tom Hamilton","sort-name":"Hamilton, Tom","gender":"male","country":"US","area":{"id":"489ce91b-6658-3307-9877-795b68554c98","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United States","sort-name":"United States","life-span":{"ended":null}},"begin-area":{"id":"6833b340-b82b-4ce6-9434-dad8da899c28","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Colorado Springs","sort-name":"Colorado Springs","life-span":{"ended":null}},"disambiguation":"bassist for Aerosmith","ipis":["00128602488"],"isnis":["0000000078387269"],"life-span":{"begin":"1951-12-31","ended":null},"aliases":[{"sort-name":"Hamilton, Thomas William","type-id":"d4dcd0c0-b341-3612-a332-c0ce797b25cf","name":"Thomas William Hamilton","locale":null,"type":"Legal name","primary":null,"begin-date":null,"end-date":null}]},{"id":"5c3c2e90-c8dd-4e03-b26d-cdbc40ed50bf","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":39,"name":"Pandora’s Box","sort-name":"Pandora’s Box","area":{"id":"26e0e534-19ea-4645-bfb3-1aa4e83a4046","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Atlanta","sort-name":"Atlanta","life-span":{"ended":null}},"begin-area":{"id":"26e0e534-19ea-4645-bfb3-1aa4e83a4046","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Atlanta","sort-name":"Atlanta","life-span":{"ended":null}},"disambiguation":"Aerosmith Tribute Band","life-span":{"begin":"2015","ended":null}},{"id":"3066b543-088d-45b5-8dd4-77f8f50c6986","score":39,"name":"Aeroforce","sort-name":"Aeroforce","disambiguation":"Aerosmith cover band","life-span":{"ended":null}},{"id":"6c30ce67-6e03-4414-8bdc-f8ce023117f0","type":"Person","type-id":"b6e035f4-3ce9-331c-97df-83397230b0df","score":36,"gender-id":"36d3d30a-839d-3eda-8cb3-29be4384e4a9","name":"Rob Bailey","sort-name":"Bailey, Rob","gender":"male","country":"US","area":{"id":"489ce91b-6658-3307-9877-795b68554c98","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United States","sort-name":"United States","life-span":{"ended":null}},"disambiguation":"guitarist - Aerosmith, Anastacia","ipis":["00675732316"],"life-span":{"ended":null},"aliases":[{"sort-name":"Bailey, Robert Brandon","type-id":"d4dcd0c0-b341-3612-a332-c0ce797b25cf","name":"Robert Brandon Bailey","locale":null,"type":"Legal name","primary":null,"begin-date":null,"end-date":null}]},{"id":"61146d40-876b-4d1a-87d4-1993518f480f","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":36,"name":"TOXIC TWINS","sort-name":"TOXIC TWINS","country":"GB","area":{"id":"8a754a16-0027-3a29-b6d7-2b40ea0481ed","type":"Country","type-id":"06dd0ae4-8c74-30bb-b43d-95dcedf961de","name":"United Kingdom","sort-name":"United Kingdom","life-span":{"ended":null}},"disambiguation":"UK Aerosmith tribute band","life-span":{"ended":null}},{"id":"14f9a04b-384a-4633-8141-20de0f92460a","score":34,"name":"Steve Gardner","sort-name":"Gardner, Steve","disambiguation":"photography on Aerosmith's Big Ones","life-span":{"ended":null}},{"id":"0fe2f5c5-6ccd-4385-9a2e-ba8858c15389","type":"Group","type-id":"e431f5f6-b5d2-343d-8b36-72607fffb74b","score":34,"name":"DeniroSmith","sort-name":"DeniroSmith","area":{"id":"58d2816b-daf9-4fc5-962c-06967f14a5e5","type":"City","type-id":"6fd8f29a-3d0a-32fc-980d-ea697b69da78","name":"Austin","sort-name":"Austin","life-span":{"ended":null}},"disambiguation":"Austin, TX Aerosmith tribute band","life-span":{"ended":null}},{"id":"1db0557b-1d2d-43d3-afda-81a67404116f","type":"Person","type-id":"b6e035f4-3ce9-331c-97df-83397230b0df","score":27,"gender-id":"93452b5a-a947-30c8-934f-6a4056b151c2","name":"Mia Tyler","sort-name":"Tyler, Mia","gender":"female","disambiguation":"The daughter of Aerosmith lead singer Steven Tyler, and actress Cyrinda Foxe.","life-span":{"ended":null}}]}"""


@pytest.fixture
def aerosmith_work_lookup():
    return """{"work-offset":0,"works":[{"languages":["eng"],"type":"Song","title":"I Wanna Know Why","id":"017a355a-7f41-3be8-8159-946d762a8018","attributes":[],"iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":""},{"title":"Animal Crackers","languages":["eng"],"type":"Song","attributes":[],"id":"052d12a0-a53d-44d4-bd3a-258ca8dd9b51","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"disambiguation":""},{"attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"360464124"},{"value":"5574001","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"}],"id":"0661eb04-50ee-47c9-b327-5d0034de88d4","type":"Song","languages":["eng"],"title":"Face","disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-071.182.655-9"]},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.033.283-7"],"attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"330476463"}],"id":"0d1e7e09-7c4f-3100-91f2-bf3f3d161bbb","languages":["eng"],"type":"Song","title":"Crazy"},{"attributes":[{"value":"330548662","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"}],"id":"140b9302-f7a7-4c13-90cb-1fe61e2c4b45","title":"Crash","type":"Song","languages":["eng"],"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.887.990-6"]},{"title":"Bitch’s Brew","languages":["eng"],"type":"Song","attributes":[],"id":"166c0e8d-6d17-4a4c-b96c-453cd0465877","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"disambiguation":""},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":"","title":"Face","type":"Song","languages":["eng"],"attributes":[],"id":"16a9b322-d2c3-4b48-acc2-41dfa2793b4a"},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.044.134-4"],"disambiguation":"","title":"Don’t Get Mad, Get Even","languages":["eng"],"type":"Song","attributes":[{"value":"GW08382139","type":"APRA ID","type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04"}],"id":"16c7f606-105d-45e3-9d68-3b273c0ba97e"},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"attributes":[],"id":"195d4944-f18c-4878-8c5c-19218e8001bd","title":"Fall Together","type":"Song","languages":["eng"]},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-010.433.701-0"],"attributes":[{"type":"SUISA ID","type-id":"034f35ae-d250-3749-95e7-854e606d5960","value":"006657 108 27"},{"type":"SPAC ID","type-id":"fd46ccc3-b3bb-4c5a-b40d-23b81899ef46","value":"5134623"},{"value":"1469248","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde","type":"SPA ID"},{"type":"SOCAN ID","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","value":"12155673"},{"type-id":"3667d8d9-9fe4-4df0-873b-4dc00a886d80","type":"SOBODAYCOM ID","value":"5134623"},{"type":"SIAE ID","type-id":"2e4f9e12-d094-4bd0-b4dd-a560cc5c4977","value":"75100135900"},{"value":"5134623","type-id":"33fe1f90-d812-48ef-839f-bc151afecaca","type":"SGACEDOM ID"},{"value":"6424182","type":"SAYCO ID","type-id":"4439b1a5-47da-4774-9caa-8b8589129297"},{"type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","type":"SAYCE ID","value":"6424182"},{"type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","type":"SADAIC ID","value":"219930"},{"type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","type":"SADAIC ID","value":"2090789"},{"value":"3267248","type":"SACVEN ID","type-id":"91064561-0c00-4574-ad18-247c7216675f"},{"type":"SACM ID","type-id":"519f3860-0b48-4532-8752-25984f662360","value":"017120823"},{"type":"SACIM ID","type-id":"43b74305-291f-4ae3-9e59-0dca457c00fe","value":"5134623"},{"type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d","type":"SABAM ID","value":"000017300"},{"value":"04548J","type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6","type":"PRS tune code"},{"value":"5134623","type":"NICAUTOR ID","type-id":"5ab3cd1a-b9ad-4244-8b32-b3a958d4ede6"},{"type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","type":"JASRAC ID","value":"0G0-9710-7"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"9946451"},{"value":"9944779","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID"},{"value":"2621751","type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641"},{"type-id":"9e0765a1-1505-3ca9-9147-8dcbb0aa9cec","type":"CASH ID","value":"C-1004232905"},{"value":"W-000036185","type-id":"a6492434-b847-4f1b-9869-9184ade990ed","type":"BUMA/STEMRA ID"},{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"370094961"},{"type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04","type":"APRA ID","value":"GW00865361"},{"type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID","value":"3157992"},{"type":"APA ID","type-id":"84afca1d-80dc-4d44-8d7a-e1e84e70aa65","value":"5243789"},{"type":"AKM ID","type-id":"eee68598-7f51-44a7-bd5a-fc728f1854d2","value":"17022201"},{"value":"6212785","type":"AGADU ID","type-id":"27ea7f59-9f9c-4aee-b31a-61109ac03819"},{"type":"AEI ID","type-id":"d12e548d-4579-4ddd-abb6-9ffa4fee6826","value":"5134623"},{"type-id":"955305a2-58ec-4c64-94f7-7fb9b209416c","type":"ACAM ID","value":"4878134"},{"type-id":"955305a2-58ec-4c64-94f7-7fb9b209416c","type":"ACAM ID","value":"5020156"},{"type-id":"955305a2-58ec-4c64-94f7-7fb9b209416c","type":"ACAM ID","value":"5134623"},{"value":"5134623","type":"AACIMH ID","type-id":"a9551cb7-ee2c-4b05-87b6-18ef0e549865"}],"id":"1df1d189-2074-305c-90de-2e9e1d90abb2","title":"Give Peace a Chance","type":"Song","languages":["eng"]},{"title":"Janie’s Got a Gun","type":"Song","languages":["eng"],"id":"22c4abfb-15ba-3462-a6fa-2125436a6ee2","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"400141913"}],"iswcs":["T-070.090.666-6"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"attributes":[],"id":"2567714d-d0b1-4194-a625-6c7388deef71","title":"Gypsy Boots","languages":["eng"],"type":"Song","disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[]},{"id":"25f1c279-8826-3070-a9f2-0e99c59ed850","attributes":[{"value":"9031104","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde","type":"SPA ID"},{"type":"SAYCO ID","type-id":"4439b1a5-47da-4774-9caa-8b8589129297","value":"842541"},{"type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9","type":"SACEM ID","value":"98 652 967 11"},{"type-id":"01eeee67-f514-3801-bdce-279e04872f91","type":"GEMA ID","value":"4265962-001"},{"value":"1690062","type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641"},{"value":"2726336","type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID"},{"value":"4448492","type-id":"27ea7f59-9f9c-4aee-b31a-61109ac03819","type":"AGADU ID"},{"type":"ACDAM ID","type-id":"f3516c3c-28d6-4a11-bfe7-64debfff342e","value":"214296"},{"value":"390600429","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"},{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"390697602"},{"value":"0I7-1348-4","type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","type":"JASRAC ID"}],"languages":["eng"],"type":"Song","title":"I Don’t Want to Miss a Thing","disambiguation":"","iswcs":["T-070.267.398-0","T-071.184.391-2"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng"},{"title":"Falling Off","languages":["eng"],"type":"Song","id":"28f76738-7609-493f-8a9f-2b649fb98b9f","attributes":[],"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.026.448-7","T-915.741.876-3"],"disambiguation":"","languages":["eng"],"type":"Song","title":"Cry Me a River","attributes":[{"value":"19070713","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID"},{"value":"000006 575 20","type-id":"034f35ae-d250-3749-95e7-854e606d5960","type":"SUISA ID"},{"value":"2730","type":"SPA ID","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde"},{"type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","type":"SOCAN ID","value":"10155855"},{"type-id":"2e4f9e12-d094-4bd0-b4dd-a560cc5c4977","type":"SIAE ID","value":"56100072400"},{"value":"182.582","type-id":"6a200b13-ca08-3b32-87a9-7aaa46368d75","type":"SGAE ID"},{"type":"SAYCO ID","type-id":"4439b1a5-47da-4774-9caa-8b8589129297","value":"752864"},{"value":"752864","type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","type":"SAYCE ID"},{"value":"203155","type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","type":"SADAIC ID"},{"type":"SACVEN ID","type-id":"91064561-0c00-4574-ad18-247c7216675f","value":"3266836"},{"type":"SACVEN ID","type-id":"91064561-0c00-4574-ad18-247c7216675f","value":"4019959"},{"value":"011189612","type-id":"519f3860-0b48-4532-8752-25984f662360","type":"SACM ID"},{"value":"67 076 990 11","type":"SACEM ID","type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9"},{"type":"SABAM ID","type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d","value":"000591700"},{"value":"6715686L","type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6","type":"PRS tune code"},{"value":"526137-001","type":"GEMA ID","type-id":"01eeee67-f514-3801-bdce-279e04872f91"},{"value":"3116457","type":"ECAD ID","type-id":"d37a8c27-8f83-47c7-954d-175d35759990"},{"value":"26449","type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"2400963"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"3235500"},{"value":"5016556","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"9239852"},{"value":"5869346","type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641"},{"type-id":"a6492434-b847-4f1b-9869-9184ade990ed","type":"BUMA/STEMRA ID","value":"W-000019798"},{"value":"GW01559052","type":"APRA ID","type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04"},{"value":"2186295","type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID"},{"type":"APA ID","type-id":"84afca1d-80dc-4d44-8d7a-e1e84e70aa65","value":"4046328"},{"type-id":"eee68598-7f51-44a7-bd5a-fc728f1854d2","type":"AKM ID","value":"2407301"},{"type":"AGADU ID","type-id":"27ea7f59-9f9c-4aee-b31a-61109ac03819","value":"4311597"},{"type-id":"955305a2-58ec-4c64-94f7-7fb9b209416c","type":"ACAM ID","value":"4046328"},{"value":"330112417","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"},{"type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","type":"JASRAC ID","value":"0C0-6680-1"}],"id":"29641434-08ec-34f5-80e2-81098b74da9a"},{"disambiguation":"","iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"2e50b517-2798-3fcc-9cac-b7cdc68c5110","attributes":[],"title":"Chip Away the Stone","type":"Song","languages":["eng"]},{"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-070.041.166-0"],"disambiguation":"Led Zeppelin version","languages":["eng"],"type":"Song","title":"Dazed and Confused","attributes":[{"value":"2143137-001","type-id":"01eeee67-f514-3801-bdce-279e04872f91","type":"GEMA ID"}],"id":"324e9a98-a968-3f27-b274-4bccc2ebe0c8"},{"disambiguation":"","iswcs":["T-070.060.316-2"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"3a07afc9-1dd4-3c37-a4ac-21e03c423684","attributes":[{"type-id":"a6492434-b847-4f1b-9869-9184ade990ed","type":"BUMA/STEMRA ID","value":"W-001107669"},{"type":"CASH ID","type-id":"9e0765a1-1505-3ca9-9147-8dcbb0aa9cec","value":"1203987372"},{"value":"10236732","type":"SOCAN ID","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d"},{"value":"236731961","type-id":"034f35ae-d250-3749-95e7-854e606d5960","type":"SUISA ID"},{"type-id":"4439b1a5-47da-4774-9caa-8b8589129297","type":"SAYCO ID","value":"782393084"},{"value":"730127061","type":"SADAIC ID","type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb"},{"value":"93177035900","type-id":"2e4f9e12-d094-4bd0-b4dd-a560cc5c4977","type":"SIAE ID"},{"type-id":"6a200b13-ca08-3b32-87a9-7aaa46368d75","type":"SGAE ID","value":"2510583"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"60317"},{"value":"2153377J","type":"PRS tune code","type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6"},{"value":"011980EV","type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6","type":"PRS tune code"},{"value":"3117905-001","type-id":"01eeee67-f514-3801-bdce-279e04872f91","type":"GEMA ID"},{"value":"AWK727267","type-id":"46f95ee0-123f-4f8e-8ccc-54c7c652dda0","type":"AMRA ID"},{"type":"SACEM ID","type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9","value":"9318391811"},{"type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d","type":"SABAM ID","value":"352395200"},{"type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d","type":"SABAM ID","value":"A0C6ADT00"},{"value":"357256","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde","type":"SPA ID"},{"type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID","value":"2218186"},{"value":"18799120","type":"ECAD ID","type-id":"d37a8c27-8f83-47c7-954d-175d35759990"},{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"360298591"}],"title":"Flesh","languages":["eng"],"type":"Song"},{"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","title":"Bone to Bone (Coney Island White Fish Boy)","languages":["eng"],"type":"Song","id":"3b3c951d-2e2c-4a43-a6fb-804e11fc4a78","attributes":[]},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":"","languages":["eng"],"type":"Song","title":"Back in the Saddle","attributes":[{"type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","value":"76447"}],"id":"3bfb1bf4-73ec-35d9-998c-8b7ce3067f41"},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":"","languages":["eng"],"type":"Song","title":"Can't Stop Messin'","attributes":[],"id":"3c7ccd37-31e0-35d8-9154-aed85fcb6a82"},{"attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"370249124"}],"id":"3ede761e-0218-3a5f-a09e-d2e77ff1bc39","languages":["eng"],"type":"Song","title":"Get a Grip","disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-071.394.994-0"]},{"type":"Song","languages":["eng"],"title":"Angel’s Eye","attributes":[],"id":"3fbe49f5-aa6d-4558-a5ea-72bb8222207f","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":""},{"iswcs":["T-070.006.694-9"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","title":"Amazing","languages":["eng"],"type":"Song","id":"417ff441-2f22-37bc-b72a-15112b55e7dc","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"310326993"}]},{"id":"4404dde4-e7ed-4364-acff-6d4ac8d42eb3","attributes":[{"value":"340541980","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"}],"languages":["eng"],"type":"Song","title":"Drop Dead Gorgeous","disambiguation":"","iswcs":["T-071.391.348-4"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6"},{"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","title":"Freedom Fighter","type":"Song","languages":["eng"],"id":"4a745480-a713-4de0-99e5-7a6afa5c8c34","attributes":[]},{"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","type":"Song","languages":["eng"],"title":"Bright Light Fright","id":"534d9fdd-4822-366c-bb2a-668f4b741ef2","attributes":[]},{"disambiguation":"","type-id":null,"language":"eng","iswcs":[],"attributes":[],"id":"5365c36d-45ca-3655-8056-41d55491f5f1","languages":["eng"],"type":null,"title":"Home Tonight"},{"title":"F.I.N.E.","languages":["eng"],"type":"Song","id":"549169b1-0271-4a11-86aa-baeeb7b86eda","attributes":[{"type":"APRA ID","type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04","value":"GW00677522"}],"iswcs":["T-915.670.434-6"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"disambiguation":"","iswcs":["T-070.278.618-2"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"59c21df2-f1a3-49af-98e8-30becab2a5ce","attributes":[],"type":"Song","languages":["eng"],"title":"Baby, Please Don’t Go"},{"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","title":"Kings and Queens","languages":["eng"],"type":"Song","id":"5c6716ed-037e-35a6-a054-6f322a08eb2f","attributes":[]},{"type":"Song","languages":["eng"],"title":"I Live in Connecticut","id":"5ce13933-cb65-4702-a31f-65e8df7fb95b","attributes":[],"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":"","title":"Adam’s Apple","languages":["eng"],"type":"Song","id":"5dffa44c-f553-3bb8-927c-719645230cce","attributes":[]},{"type":"Song","languages":["eng"],"title":"Dude (Looks Like a Lady)","attributes":[{"value":"340276455","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"},{"value":"886602868","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"}],"id":"604fe801-4608-3c31-8cee-4291ac711dcb","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.043.592-2","T-915.670.438-0"],"disambiguation":""},{"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":null,"disambiguation":"","languages":[],"type":"Song","title":"Bacon Biscuit Blues","id":"63f9f0b3-6a75-4357-86a4-5f4dd2adc9d6","attributes":[]},{"iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":"","languages":["eng"],"type":"Song","title":"Head First","id":"6418bd8f-684b-4852-b02d-eb12e843033a","attributes":[]},{"disambiguation":"","iswcs":["T-070.265.558-0"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","id":"686646e6-e0be-4ef0-8a51-e6f1a227fe4a","attributes":[{"value":"380387500","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"}],"title":"Hole in My Soul","languages":["eng"],"type":"Song"},{"disambiguation":"","iswcs":["T-070.005.393-5"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"6888ee03-be1a-3682-a57c-cce4685028e7","attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"310262061"}],"languages":["eng"],"type":"Song","title":"Angel"},{"disambiguation":"","iswcs":["T-070.259.809-1"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"6bbaadef-a97f-3d2f-9573-0d46562505e5","attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"330476481"}],"title":"Cryin’","type":"Song","languages":["eng"]},{"disambiguation":"","iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","id":"6bd710c2-0c89-3243-b6d6-cb87ae4a4211","attributes":[{"value":"832977","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"}],"languages":["eng"],"type":"Song","title":"Last Child"},{"id":"6cd8e825-e348-4195-ad96-0643df5929d8","attributes":[],"type":"Song","languages":["eng"],"title":"Darkness","disambiguation":"","iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng"},{"languages":["eng"],"type":"Song","title":"Get It Up","attributes":[],"id":"6cf5718e-12ac-3e52-94e4-cf5d46378ef0","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":""},{"languages":["eng"],"type":"Song","title":"I Never Loved a Man (The Way I Love You)","attributes":[],"id":"6db2282e-a618-371c-82ea-4a8482f71394","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":""},{"id":"70d53de1-86ac-4122-bc36-81e192fad945","attributes":[{"value":"380283854","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"},{"type":"SOCAN ID","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","value":"10751716"},{"type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","type":"JASRAC ID","value":"0H2-0143-9"}],"languages":["eng"],"type":"Song","title":"Hangman Jury","disambiguation":"","iswcs":["T-071.982.587-2"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng"},{"title":"Closer","languages":["eng"],"type":"Song","id":"70e1eefe-d6e8-467a-96e5-e08e1fe73d6e","attributes":[{"type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","value":"15052671"}],"iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":""},{"attributes":[],"id":"71fc865d-c011-4f0d-ab40-bd727da498b2","title":"Don’t Stop","type":"Song","languages":["eng"],"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[]},{"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-070.231.926-3"],"attributes":[],"id":"74ad5d61-c7a8-3e5e-82cd-765c5ecd0d17","title":"All Your Love (I Miss Loving)","type":"Song","languages":["eng"]},{"title":"Avant Garden","languages":["eng"],"type":"Song","attributes":[{"type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","value":"5611264"},{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"310503489"}],"id":"7c58c514-0e36-4667-8294-703188c96f70","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-071.179.988-0"],"disambiguation":""},{"disambiguation":"","iswcs":["T-070.069.465-0"],"language":"eng","type-id":null,"id":"7f4362ed-2da6-47b7-b9ac-5366f2564c7b","attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"370272296"}],"title":"Going Down","languages":["eng"],"type":null},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-910.693.638-3"],"attributes":[{"value":"884233521","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"},{"type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID","value":"15052670"}],"id":"815b3adf-0329-4636-b118-7e314c8c1b93","title":"Can’t Stop Lovin’ You","type":"Song","languages":["eng"]},{"title":"I'm Not Talking","languages":["eng"],"type":"Song","id":"8257bb21-fea8-3c49-bd7c-081423873f92","attributes":[{"type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","value":"662349"}],"iswcs":["T-700.027.054-4"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":""},{"languages":["eng"],"type":"Song","title":"Kiss Your Past Good‐Bye","id":"836cf667-0998-4f95-9fd7-618cc53067ae","attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"410120328"}],"iswcs":["T-070.268.296-9"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":""},{"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"attributes":[],"id":"8556ff3d-619e-463c-9de2-77b2d6364114","type":"Song","languages":["eng"],"title":"Jig Is Up"},{"title":"Lay It Down","type":"Song","languages":["eng"],"attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"420806891"}],"id":"876af2e2-91d2-46d4-9c74-7b55dc4fb67a","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-073.109.788-3"],"disambiguation":""},{"disambiguation":"","iswcs":["T-070.085.694-5"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","id":"8770d5c6-0fa6-3948-a70f-af79e187793f","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"390521621"}],"languages":["eng"],"type":"Song","title":"Intro"},{"id":"89fd0e60-5c44-471d-9daa-cc9e50533164","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"884233513"},{"type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","value":"15052669"}],"type":"Song","languages":["eng"],"title":"Beautiful","disambiguation":"","iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng"},{"disambiguation":"","iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"8d526d4c-e390-4b3c-bff9-4d4c3424668c","attributes":[],"title":"Chiquita","languages":["eng"],"type":"Song"},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.061.251-6"],"attributes":[{"value":"360339966","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"}],"id":"8ead4bfe-aa24-401b-8ecb-f8c344fa64e4","languages":["eng"],"type":"Song","title":"Falling in Love (Is Hard on the Knees)"},{"languages":["eng"],"type":"Song","title":"Full Circle","id":"8f7fc0fd-2c9e-446e-bca0-73a307251f19","attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"360343139"}],"iswcs":["T-070.263.289-0"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":""},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-071.060.035-3"],"disambiguation":"","title":"Deuces Are Wild","languages":["eng"],"type":"Song","attributes":[{"value":"340350132","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"}],"id":"908b0c6a-2866-30d5-8912-e98f17c86050"},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"zxx","iswcs":[],"attributes":[],"id":"92717ba6-244c-42c0-91d6-8fec05240c9d","type":"Song","languages":["zxx"],"title":"Krawhitham"},{"title":"Bolivian Ragamuffin","type":"Song","languages":["eng"],"id":"92a4cd04-a2fb-46d5-a7ba-efdc068610ce","attributes":[],"iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":""},{"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"disambiguation":"","type":"Song","languages":["eng"],"title":"Joanie's Butterfly","attributes":[],"id":"992dc86b-a9fa-416e-a73b-44244a751557"},{"id":"9be9b179-0ba0-37d7-bfd0-42fa8e603111","attributes":[],"title":"Combination","type":"Song","languages":["eng"],"disambiguation":"","iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng"},{"attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"341986418"}],"id":"9c4a9d2c-a3fb-4d09-9db8-7f3c9a5a2937","type":"Song","languages":["eng"],"title":"Devil’s Got a New Disguise","disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-072.821.846-9"]},{"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"attributes":[],"id":"a2190ff2-6184-4d2d-85a8-453ce8661239","title":"Downtown Charlie","type":"Song","languages":["eng"]},{"title":"Cheese Cake","languages":["eng"],"type":"Song","id":"a2529947-a88c-42c3-8223-c06bfa01a75f","attributes":[],"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-915.014.760-7"],"disambiguation":"","languages":["eng"],"type":"Song","title":"Another Last Goodbye","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"884360940"}],"id":"a5611017-e17d-4458-af37-e8fb3e0983c4"},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-071.184.628-4"],"attributes":[{"value":"400217967","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"}],"id":"abb56685-e911-46b0-bf55-7a951abd0838","title":"Just Push Play","type":"Song","languages":["eng"]},{"iswcs":["T-070.080.286-3"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","languages":["eng"],"type":"Song","title":"Immigrant Song","id":"b38119e8-260f-372c-a1ca-653d02b5577c","attributes":[{"value":"390244458","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"}]},{"disambiguation":"","iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"b4c537b6-b793-44a3-af77-c0dc8316fb27","attributes":[{"value":"4574013","type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479"}],"title":"Fly Away From Here","type":"Song","languages":["eng"]},{"language":"zxx","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"disambiguation":"","type":"Song","languages":["zxx"],"title":"Circle Jerk","attributes":[],"id":"b5c6b07e-5b16-4f8a-bcc1-ac88e43a6316"},{"disambiguation":"","iswcs":["T-071.180.545-6"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","id":"b648a3e6-b333-49a3-81c5-cc71465f7cd9","attributes":[{"value":"320605449","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"},{"value":"5611314","type":"BMI ID","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479"}],"title":"Beyond Beautiful","type":"Song","languages":["eng"]},{"type":"Song","languages":["eng"],"title":"Ain’t That a Bitch","id":"b83a5cc0-88a0-49b2-84f4-5672bd33b115","attributes":[{"value":"310394044","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"}],"iswcs":["T-070.256.951-4"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"attributes":[],"id":"bb82065a-df87-4b20-8d51-67ab0cfe56f1","title":"Jesus Is on the Main Line","languages":["eng"],"type":"Song","disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[]},{"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"attributes":[{"value":"660220515","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"}],"id":"bbf5781e-214f-4100-8e2a-68bb04a76ab5","languages":["eng"],"type":"Song","title":"Fallen Angels"},{"disambiguation":"","iswcs":["T-900.009.636-6"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"bcd490e5-dac7-3b8a-b423-ae17e1209f3d","attributes":[{"value":"233082","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"},{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"330174155"},{"type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04","type":"APRA ID","value":"GW01110002"},{"value":"1973254","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID"},{"type-id":"034f35ae-d250-3749-95e7-854e606d5960","type":"SUISA ID","value":"000015 175 35"},{"type":"SGAE ID","type-id":"6a200b13-ca08-3b32-87a9-7aaa46368d75","value":"2.068.129"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"10083367"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"4640548"},{"value":"12078201","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","type":"SOCAN ID"},{"type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","type":"JASRAC ID","value":"0C2-2090-7"},{"type-id":"d37a8c27-8f83-47c7-954d-175d35759990","type":"ECAD ID","value":"15193"},{"type":"GEMA ID","type-id":"01eeee67-f514-3801-bdce-279e04872f91","value":"633229-001"},{"value":"5244693","type-id":"84afca1d-80dc-4d44-8d7a-e1e84e70aa65","type":"APA ID"},{"type":"SIAE ID","type-id":"2e4f9e12-d094-4bd0-b4dd-a560cc5c4977","value":"77200105900"},{"type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","type":"SADAIC ID","value":"231886"},{"type-id":"955305a2-58ec-4c64-94f7-7fb9b209416c","type":"ACAM ID","value":"3383356"},{"value":"3269241","type":"SACVEN ID","type-id":"91064561-0c00-4574-ad18-247c7216675f"},{"value":"17762101","type-id":"eee68598-7f51-44a7-bd5a-fc728f1854d2","type":"AKM ID"},{"type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID","value":"9008973"},{"value":"1366459","type-id":"4439b1a5-47da-4774-9caa-8b8589129297","type":"SAYCO ID"},{"value":"1366459","type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","type":"SAYCE ID"},{"type-id":"27ea7f59-9f9c-4aee-b31a-61109ac03819","type":"AGADU ID","value":"6066810"},{"value":"001127900","type":"SABAM ID","type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d"},{"value":"62 084 587 11","type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9","type":"SACEM ID"},{"value":"W-000239681","type-id":"a6492434-b847-4f1b-9869-9184ade990ed","type":"BUMA/STEMRA ID"},{"type-id":"519f3860-0b48-4532-8752-25984f662360","type":"SACM ID","value":"015589179"},{"type":"SOCAN ID","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","value":"71823369"},{"type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6","type":"PRS tune code","value":"34944A"}],"type":"Song","languages":["eng"],"title":"Come Together"},{"id":"be9410cb-d8a3-4bd5-813b-d663c7c27fe6","attributes":[{"value":"6160814","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"},{"value":"370423828","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"},{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"370503312"}],"languages":["eng"],"type":"Song","title":"Girls of Summer","disambiguation":"","iswcs":["T-071.877.377-3"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng"},{"iswcs":[],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","title":"Critical Mass","languages":["eng"],"type":"Song","id":"beac9430-774e-3f0c-b202-bf1fea1845ad","attributes":[]},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-071.141.058-0","T-901.127.233-6"],"disambiguation":"","type":"Song","languages":["eng"],"title":"I Ain’t Got You","attributes":[{"value":"000388 416 62","type-id":"034f35ae-d250-3749-95e7-854e606d5960","type":"SUISA ID"},{"type":"SPA ID","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde","value":"47282"},{"value":"21611176","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","type":"SOCAN ID"},{"value":"12197639","type":"SOCAN ID","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d"},{"type":"SGAE ID","type-id":"6a200b13-ca08-3b32-87a9-7aaa46368d75","value":"131.224"},{"type-id":"4439b1a5-47da-4774-9caa-8b8589129297","type":"SAYCO ID","value":"1189900"},{"value":"10700888","type":"SAYCO ID","type-id":"4439b1a5-47da-4774-9caa-8b8589129297"},{"type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","type":"SAYCE ID","value":"1189900"},{"type":"SAYCE ID","type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","value":"10700888"},{"value":"1947686","type":"SADAIC ID","type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb"},{"type":"SADAIC ID","type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","value":"709326"},{"value":"2212730","type-id":"91064561-0c00-4574-ad18-247c7216675f","type":"SACVEN ID"},{"value":"063701200","type":"SACM ID","type-id":"519f3860-0b48-4532-8752-25984f662360"},{"value":"82 312 175 11","type":"SACEM ID","type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9"},{"type":"SABAM ID","type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d","value":"004222900"},{"value":"288587850","type":"SABAM ID","type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d"},{"type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6","type":"PRS tune code","value":"4960DT"},{"type":"OSA ID","type-id":"2f9f66e4-c32c-4215-b7f4-4a146d0b2a0e","value":"I000.32.25.24"},{"type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","type":"JASRAC ID","value":"0I0-7426-1"},{"value":"398863-001","type-id":"01eeee67-f514-3801-bdce-279e04872f91","type":"GEMA ID"},{"value":"2599159","type":"ECAD ID","type-id":"d37a8c27-8f83-47c7-954d-175d35759990"},{"value":"14743789","type":"ECAD ID","type-id":"d37a8c27-8f83-47c7-954d-175d35759990"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"1623032"},{"value":"W-000210058","type-id":"a6492434-b847-4f1b-9869-9184ade990ed","type":"BUMA/STEMRA ID"},{"value":"601950","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"},{"value":"GW02688804","type":"APRA ID","type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04"},{"value":"5921843","type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID"},{"value":"37208701","type":"AKM ID","type-id":"eee68598-7f51-44a7-bd5a-fc728f1854d2"},{"value":"5441042","type":"AGADU ID","type-id":"27ea7f59-9f9c-4aee-b31a-61109ac03819"}],"id":"c2e937dd-1a9c-3911-927d-0f92a9f8c9de"},{"id":"c364a08d-a344-3869-b72f-da24081d6a0c","attributes":[],"title":"Get the Lead Out","type":"Song","languages":["eng"],"disambiguation":"","iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6"},{"iswcs":["T-070.881.238-7"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","languages":["eng"],"type":"Song","title":"Dream On","id":"cd0b9256-79e9-338b-9c93-51e309e2ff00","attributes":[{"value":"1381435","type":"GEMA ID","type-id":"01eeee67-f514-3801-bdce-279e04872f91"},{"value":"341548","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"},{"type":"JASRAC ID","type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c","value":"0D0-6618-9"}]},{"type":"Song","languages":["eng"],"title":"Girl Keeps Coming Apart","attributes":[],"id":"cd91c65f-0f5b-4775-9407-b21d46ba5900","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"disambiguation":""},{"iswcs":["T-070.060.281-8"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":"","title":"Fever","type":"Song","languages":["eng"],"id":"cec28c07-153e-3ddb-bec5-2ef5211563ea","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"360296977"}]},{"disambiguation":"","iswcs":["T-071.060.784-3"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"d5482670-d7c4-3857-acaa-0c97416e0d84","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"350191394"}],"languages":["eng"],"type":"Song","title":"Eat the Rich"},{"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-010.436.831-1"],"disambiguation":"","languages":["eng"],"type":"Song","title":"I’m Down","attributes":[{"type":"SUISA ID","type-id":"034f35ae-d250-3749-95e7-854e606d5960","value":"000015 215 49"},{"value":"5395","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde","type":"SPA ID"},{"type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","type":"SOCAN ID","value":"12212099"},{"value":"75100165500","type":"SIAE ID","type-id":"2e4f9e12-d094-4bd0-b4dd-a560cc5c4977"},{"type-id":"6a200b13-ca08-3b32-87a9-7aaa46368d75","type":"SGAE ID","value":"32.364"},{"type":"SAYCO ID","type-id":"4439b1a5-47da-4774-9caa-8b8589129297","value":"1630260"},{"value":"1630260","type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","type":"SAYCE ID"},{"type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","type":"SADAIC ID","value":"212628"},{"value":"3445774","type":"SACVEN ID","type-id":"91064561-0c00-4574-ad18-247c7216675f"},{"value":"009887937","type-id":"519f3860-0b48-4532-8752-25984f662360","type":"SACM ID"},{"type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9","type":"SACEM ID","value":"64 223 368 11"},{"value":"003390000","type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d","type":"SABAM ID"},{"value":"39671N","type":"PRS tune code","type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6"},{"type":"GEMA ID","type-id":"01eeee67-f514-3801-bdce-279e04872f91","value":"634417-001"},{"value":"25534","type-id":"d37a8c27-8f83-47c7-954d-175d35759990","type":"ECAD ID"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"6965913"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"11261299"},{"value":"W-000238489","type":"BUMA/STEMRA ID","type-id":"a6492434-b847-4f1b-9869-9184ade990ed"},{"value":"390212723","type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76"},{"type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04","type":"APRA ID","value":"GW00750956"},{"type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID","value":"3325365"},{"value":"5243440","type-id":"84afca1d-80dc-4d44-8d7a-e1e84e70aa65","type":"APA ID"},{"value":"6884201","type-id":"eee68598-7f51-44a7-bd5a-fc728f1854d2","type":"AKM ID"},{"type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID","value":"652311"}],"id":"d8275eaa-dd65-32b1-9808-88ed02ba99b9"},{"languages":["eng"],"type":"Song","title":"Blind Man","id":"d9132dfe-e8c2-351a-a80e-fcb2c78bcd30","attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"320418893"}],"iswcs":["T-070.018.971-4"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","disambiguation":""},{"id":"da5b679c-9e91-3241-a959-f43a224f54be","attributes":[{"value":"000087 233 96","type-id":"034f35ae-d250-3749-95e7-854e606d5960","type":"SUISA ID"},{"value":"5039","type":"SPA ID","type-id":"f15e9a05-231e-415b-8b7d-8ec44c736bde"},{"value":"12181803","type-id":"00370bc6-7388-34a1-aa5b-b0e081ccb53d","type":"SOCAN ID"},{"value":"75100150200","type-id":"2e4f9e12-d094-4bd0-b4dd-a560cc5c4977","type":"SIAE ID"},{"value":"60.081","type-id":"6a200b13-ca08-3b32-87a9-7aaa46368d75","type":"SGAE ID"},{"value":"1357935","type-id":"4439b1a5-47da-4774-9caa-8b8589129297","type":"SAYCO ID"},{"type-id":"4bcbb212-a973-46ee-8160-e9f0f89ca6ef","type":"SAYCE ID","value":"1357935"},{"type-id":"d8aa6512-314a-478d-bd30-e8d7411509cb","type":"SADAIC ID","value":"218279"},{"value":"3323406","type-id":"91064561-0c00-4574-ad18-247c7216675f","type":"SACVEN ID"},{"type-id":"519f3860-0b48-4532-8752-25984f662360","type":"SACM ID","value":"011624234"},{"type-id":"d08147e2-d167-3c2b-bbe4-af6643a194f9","type":"SACEM ID","value":"63 985 142 11"},{"value":"004964800","type":"SABAM ID","type-id":"b2e4067b-a9e4-44fb-bfcd-3c1fc41dc06d"},{"value":"11704P","type-id":"cd76035f-b94e-4e47-850d-aa2cd825d1a6","type":"PRS tune code"},{"value":"634072-001","type-id":"01eeee67-f514-3801-bdce-279e04872f91","type":"GEMA ID"},{"value":"2104","type-id":"d37a8c27-8f83-47c7-954d-175d35759990","type":"ECAD ID"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"2188020"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"2545320"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"7231188"},{"value":"7712714","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID"},{"type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID","value":"10617777"},{"type":"COMPASS ID","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","value":"10045954"},{"value":"9950426","type-id":"5ea37343-be89-4cd0-8a37-f471738df641","type":"COMPASS ID"},{"value":"W-000238195","type":"BUMA/STEMRA ID","type-id":"a6492434-b847-4f1b-9869-9184ade990ed"},{"value":"380131884","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID"},{"type":"APRA ID","type-id":"bdf50af8-0881-322a-8ab7-f031a4935d04","value":"GW00645402"},{"type-id":"b66d6f47-2a58-4bc1-ac75-b44c47a198f8","type":"APDAYC ID","value":"3157996"},{"type-id":"84afca1d-80dc-4d44-8d7a-e1e84e70aa65","type":"APA ID","value":"5243737"},{"value":"15864901","type-id":"eee68598-7f51-44a7-bd5a-fc728f1854d2","type":"AKM ID"},{"value":"6027772","type-id":"27ea7f59-9f9c-4aee-b31a-61109ac03819","type":"AGADU ID"},{"value":"0H1-1330-1","type":"JASRAC ID","type-id":"31048fcc-3dbb-3979-8f85-805afb933e0c"},{"type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID","value":"555707"}],"type":"Song","languages":["eng"],"title":"Helter Skelter","disambiguation":"","iswcs":["T-010.434.396-5"],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6"},{"attributes":[],"id":"dd0376a3-f337-4493-b7ba-b891f559745b","title":"Ain’t Enough","languages":["eng"],"type":"Song","disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[]},{"attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"370253388"}],"id":"dd3c692f-61da-359a-89ad-4aa67c896282","title":"Gotta Love It","type":"Song","languages":["eng"],"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-070.264.332-0"]},{"disambiguation":"","language":"zxx","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":["T-070.018.881-3"],"attributes":[{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"320415636"}],"id":"df5be4fc-70cf-3924-a465-f9bbf72819fc","languages":["zxx"],"type":"Song","title":"Boogie Man"},{"iswcs":[],"language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","disambiguation":"","languages":["eng"],"type":"Song","title":"Draw the Line","id":"e1b785ef-fbc7-3a6a-bda5-6789f96f5765","attributes":[]},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"attributes":[],"id":"e7a7b706-1876-35eb-91ce-bd12c2c4d31f","title":"Big Ten Inch Record","type":"Song","languages":["eng"]},{"attributes":[{"value":"5477534","type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID"},{"type":"ASCAP ID","type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","value":"400213649"}],"id":"e9007b9d-8417-40ea-b5ba-56080e4d5167","languages":["eng"],"type":"Song","title":"Jaded","disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-071.065.253-1"]},{"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[],"attributes":[],"id":"ef049799-aed8-4880-99ee-30ec7529af19","type":"Song","languages":["eng"],"title":"Heart’s Done Time"},{"attributes":[],"id":"f1893c0c-b9e6-311f-9c97-28f26beeea66","title":"Eyesight to the Blind","type":null,"languages":["eng"],"disambiguation":"","language":"eng","type-id":null,"iswcs":["T-903.944.909-7"]},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":[],"attributes":[],"id":"f4d3ddd6-8365-4eca-830b-dc46c78d2996","languages":["eng"],"type":"Song","title":"Jailbait"},{"disambiguation":"","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","iswcs":["T-070.240.801-2"],"attributes":[],"id":"f80959bc-b85e-399a-b5de-a182a40c126d","title":"I’m Ready","languages":["eng"],"type":"Song"},{"attributes":[],"id":"fca0b662-f236-46ec-972b-720e50c375bd","title":"Back Back Train","type":"Song","languages":["eng"],"disambiguation":"","language":"eng","type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","iswcs":[]},{"disambiguation":"","iswcs":["T-070.883.006-1"],"type-id":"f061270a-2fd6-32f1-a641-f0f8676d14e6","language":"eng","id":"fd71f853-a87d-4a1f-96ca-38979c9e8a9f","attributes":[{"type-id":"d833318c-6c6a-370e-8b16-9cb15873ba76","type":"ASCAP ID","value":"880224201"},{"type-id":"c8ac7e55-fd45-3002-9f89-e8b43911a479","type":"BMI ID","value":"3931209"}],"title":"Attitude Adjustment","languages":["eng"],"type":"Song"}],"work-count":100}"""


@pytest.fixture
def aerosmith_work_link():
    return "https://musicbrainz.org/ws/2/work?artist=3d2b98e5-556f-4451-a3ff-c50ea18d57cb&limit=100&offset=0&fmt=json"


@pytest.fixture
def aerosmith_last_page_work_link():
    return "https://musicbrainz.org/ws/2/work?artist=3d2b98e5-556f-4451-a3ff-c50ea18d57cb&limit=100&offset=100&fmt=json"


@pytest.fixture
def aerosmith_last_page_work_payload():
    return '{"work-offset":100,"works":[],"work-count":100}'


@pytest.fixture
def mbo_test():
    mbo = MusicBrainzOrg("testagent.com")
    return mbo


@pytest.fixture
def mock_aerosmith(requests_mock, aerosmith_link, aerosmith_lookup):
    requests_mock.get(aerosmith_link,
                      text=aerosmith_lookup)
    return requests_mock


@pytest.fixture
def aerosmith_id():
    return "3d2b98e5-556f-4451-a3ff-c50ea18d57cb"


@pytest.fixture
def lyricsovg_artist():
    return "Coldplay"


@pytest.fixture
def lyricsovg_title():
    return "Adventure of a Lifetime"


@pytest.fixture
def lyticsovg_link():
    return 'https://api.lyrics.ovh/v1/Coldplay/Adventure%20of%20a%20Lifetime'


@pytest.fixture
def lyricsovg_song_result():
    return '{"lyrics":"Coldplay\\r\\nTurn your magic on"}'


@pytest.fixture
def lyricsovg_expected_text():
    return """Turn your magic on"""


@pytest.fixture
def aerosmith_works_mock_request(requests_mock,
                                 aerosmith_work_link,
                                 aerosmith_work_lookup,
                                 aerosmith_last_page_work_link,
                                 aerosmith_last_page_work_payload):

    requests_mock.get(aerosmith_work_link, text=aerosmith_work_lookup)
    requests_mock.get(aerosmith_last_page_work_link,
                      text=aerosmith_last_page_work_payload)
    return requests_mock


@pytest.fixture
def aerosmith_works_song_names():
    return ['I Wanna Know Why', 'Animal Crackers', 'Face', 'Crazy', 'Crash', 'Bitch’s Brew', 'Face', 'Don’t Get Mad, Get Even', 'Fall Together', 'Give Peace a Chance', 'Janie’s Got a Gun', 'Gypsy Boots', 'I Don’t Want to Miss a Thing', 'Falling Off', 'Cry Me a River', 'Chip Away the Stone', 'Dazed and Confused', 'Flesh', 'Bone to Bone (Coney Island White Fish Boy)', 'Back in the Saddle', "Can't Stop Messin'", 'Get a Grip', 'Angel’s Eye', 'Amazing', 'Drop Dead Gorgeous', 'Freedom Fighter', 'Bright Light Fright', 'F.I.N.E.', 'Baby, Please Don’t Go', 'Kings and Queens', 'I Live in Connecticut', 'Adam’s Apple', 'Dude (Looks Like a Lady)', 'Bacon Biscuit Blues', 'Head First', 'Hole in My Soul', 'Angel', 'Cryin’', 'Last Child', 'Darkness', 'Get It Up', 'I Never Loved a Man (The Way I Love You)', 'Hangman Jury', 'Closer', 'Don’t Stop', 'All Your Love (I Miss Loving)', 'Avant Garden', 'Can’t Stop Lovin’ You', "I'm Not Talking", 'Kiss Your Past Good‐Bye', 'Jig Is Up', 'Lay It Down', 'Intro', 'Beautiful', 'Chiquita', 'Falling in Love (Is Hard on the Knees)', 'Full Circle', 'Deuces Are Wild', 'Krawhitham', 'Bolivian Ragamuffin', "Joanie's Butterfly", 'Combination', 'Devil’s Got a New Disguise', 'Downtown Charlie', 'Cheese Cake', 'Another Last Goodbye', 'Just Push Play', 'Immigrant Song', 'Fly Away From Here', 'Circle Jerk', 'Beyond Beautiful', 'Ain’t That a Bitch', 'Jesus Is on the Main Line', 'Fallen Angels', 'Come Together', 'Girls of Summer', 'Critical Mass', 'I Ain’t Got You', 'Get the Lead Out', 'Dream On', 'Girl Keeps Coming Apart', 'Fever', 'Eat the Rich', 'I’m Down', 'Blind Man', 'Helter Skelter', 'Ain’t Enough', 'Gotta Love It', 'Boogie Man', 'Draw the Line', 'Big Ten Inch Record', 'Jaded', 'Heart’s Done Time', 'Jailbait', 'I’m Ready', 'Back Back Train', 'Attitude Adjustment']


@pytest.fixture
def end_to_end_artist():
    return "asd"


@pytest.fixture
def end_to_end_mokup(requests_mock, end_to_end_artist):
    # https://api.lyrics.ovh/v1/asd/asdasd
    # https://musicbrainz.org/ws/2/work?artist=a18d57cb&limit=100&offset=0&fmt=json
    # https://musicbrainz.org/ws/2/artist?query=asd&limit=100&offset=0&fmt=json

    requests_mock.get("https://musicbrainz.org/ws/2/artist?query=asd&limit=100&offset=0&fmt=json",
                      text="""{"created":"2021-11-26T10:19:25.458Z","count":1,"offset":0,"artists":[{"id":"a18d57cb","name":"asd","country":"US"}]}""")

    requests_mock.get("https://musicbrainz.org/ws/2/work?artist=a18d57cb&limit=100&offset=0&fmt=json",
                      text="""{"work-offset":0,"works":[{"languages":["eng"],"type":"Song","title":"asd takes"},{"title":"Crackers","languages":["eng"],"type":"Song"}],"work-count":2}""")

    requests_mock.get("https://musicbrainz.org/ws/2/work?artist=a18d57cb&limit=100&offset=100&fmt=json",
                      text="""{"work-offset":0,"works":[],"work-count":2}""")

    requests_mock.get("https://api.lyrics.ovh/v1/asd/asd%20takes",
                      text="""{"lyrics":"abc\\r\\nTurn your magic on"}""")

    requests_mock.get("https://api.lyrics.ovh/v1/asd/Crackers",
                      text="""{"lyrics":"abc\\r\\n magic on"}""")

    return requests_mock


@pytest.fixture
def end_to_end_mokup_2(requests_mock, end_to_end_artist):
    # https://api.lyrics.ovh/v1/asd/asdasd
    # https://musicbrainz.org/ws/2/work?artist=a18d57cb&limit=100&offset=0&fmt=json
    # https://musicbrainz.org/ws/2/artist?query=asd&limit=100&offset=0&fmt=json

    requests_mock.get("https://musicbrainz.org/ws/2/artist?query=asd&limit=100&offset=0&fmt=json",
                      text="""{"created":"2021-11-26T10:19:25.458Z","count":2,"offset":0,"artists":[{"id":"a18d57cb","name":"asd","country":"US"},{"id":"a1857cb","name":"asd asd","country":"US"}]}""")

    requests_mock.get("https://musicbrainz.org/ws/2/work?artist=a18d57cb&limit=100&offset=0&fmt=json",
                      text="""{"work-offset":0,"works":[{"languages":["eng"],"type":"Song","title":"asd takes"},{"title":"Crackers","languages":["eng"],"type":"Song"}],"work-count":2}""")

    requests_mock.get("https://musicbrainz.org/ws/2/work?artist=a18d57cb&limit=100&offset=100&fmt=json",
                      text="""{"work-offset":0,"works":[],"work-count":2}""")

    requests_mock.get("https://api.lyrics.ovh/v1/asd/asd%20takes",
                      text="""{"lyrics":"abc\\r\\nTurn your magic on"}""")

    requests_mock.get("https://api.lyrics.ovh/v1/asd/Crackers",
                      text="""{"error":"abc\\r\\n magic on"}""")

    return requests_mock


@pytest.fixture
def end_to_end_expected_result_2(end_to_end_artist):

    return 4.0


@pytest.fixture
def end_to_end_expected_result(end_to_end_artist):

    return 3.0


@pytest.fixture
def lyricsOVH_test():
    return LyricsOVH()


class TestMusicBrainzOrg:
    """[summary]
    """

    def test_addresses(self):
        assert MusicBrainzOrg.AUTHOR_QUERY_URL == "https://musicbrainz.org/ws/2/artist"
        assert MusicBrainzOrg.AUTHOR_WORK_QUERY_URL == "https://musicbrainz.org/ws/2/work"

    def test_search_author(self,
                           mock_aerosmith,
                           aerosmith_name,
                           mbo_test):
        """

        """
        result = mbo_test.search_author(aerosmith_name)

        assert len(result["artists"]) == 14

    def test_author_generator(self,
                              mock_aerosmith,
                              aerosmith_name,
                              mbo_test):
        """
        """

        list_of_authors = list(mbo_test.author_generator(aerosmith_name))

        assert len(list_of_authors) == 14

    def test_get_first_author_id(self,
                                 mock_aerosmith,
                                 aerosmith_name,
                                 mbo_test,
                                 aerosmith_id):

        assert aerosmith_id == mbo_test.get_first_author_id(aerosmith_name)

    def test_song_name_generator(self,
                                 aerosmith_id,
                                 mbo_test,
                                 aerosmith_works_mock_request,
                                 aerosmith_works_song_names):
        """
        check that is gathering all the name available with
        the type "Song"

        Args:
            requests_mock ([type]): [description]
            aerosmith_work_link ([type]): [description]
            aerosmith_work_lookup ([type]): [description]
            aerosmith_id ([type]): [description]
            mbo_test ([type]): [description]

        Raises:
            Exception: [description]
        """

        result = [f for f in mbo_test.song_name_generator(aerosmith_id)]

        assert len(aerosmith_works_song_names) == len(result)

    def test_fetch_work(self,
                        requests_mock,
                        aerosmith_work_link,
                        aerosmith_work_lookup,
                        aerosmith_id,
                        mbo_test):
        """[summary]
        """
        requests_mock.get(aerosmith_work_link, text=aerosmith_work_lookup)
        result = mbo_test.fetch_work(aerosmith_id)
        assert "works" in result
        assert "work-offset" in result
        assert "work-count" in result
        assert 100 == result["work-count"]
        assert result["work-offset"] == 0


class TestLyricsOVH:
    """[summary]
    """

    def test_addresses(self, lyricsOVH_test):
        base = lyricsOVH_test.LOOKUP_ARTIST_LYRICS_FORMAT
        author = "a"
        title = "b"
        result = 'https://api.lyrics.ovh/v1/a/b'
        assert base.format(author, title) == result

    def test_parse_url(self, lyricsOVH_test):
        """[summary]
        """
        artist = "Coldplay"
        title = "Adventure of a Lifetime"
        result = "https://api.lyrics.ovh/v1/Coldplay/Adventure%20of%20a%20Lifetime"
        assert result == lyricsOVH_test.parse_url(artist, title)

    def test_search_artist_lyric_by_title(self,
                                          requests_mock,
                                          lyricsOVH_test,
                                          lyticsovg_link,
                                          lyricsovg_song_result,
                                          lyricsovg_artist,
                                          lyricsovg_title,
                                          lyricsovg_expected_text):
        """[summary]
        """
        requests_mock.get(lyticsovg_link, text=lyricsovg_song_result)
        song = lyricsOVH_test.search_artist_lyric_by_title(
            lyricsovg_artist, lyricsovg_title)

        assert lyricsovg_expected_text == song


class TestFirstElementMenuSelector:

    def test_select(self,
                    mbo_test,
                    real_artist,
                    madonna_id):
        menu = FirstElementMenuSelector(mbo_test)

        assert madonna_id == menu.select(real_artist)


class TestTerminalMenuSelector:

    def test_extract_features(self,
                              mbo_test,
                              real_artist,
                              madonna_id):
        menu = TerminalMenuSelector(mbo_test)
        authors_id, menu_list = menu.extract_features(real_artist)

        assert madonna_id in authors_id.values()
        assert len(authors_id) == len(menu_list)
        assert authors_id[0] == madonna_id
        assert 'Madonna Person US American singer-songwriter, actress, businesswoman, “Queen of Pop”' == menu_list[0]

    @pytest.mark.skip(reason="The test is not possible to automate without breaking the encapsulation of the library, and refactoring the code.")
    def test_select(self,
                    mbo_test,
                    real_artist,
                    madonna_id,
                    monkeypatch):
        """ Test terminal selection

        The test is not possible to automate.
        The TerminalMenu classs creates a new terminal and substitutes the 
        builtins stdin, and stdout (simple_term_menu.py, line 865, _init_term). 
        The new terminal is created inside the show() method, thus it would be 
        required to refactor the library to expose the new terminal.
        The new terminal substitutes the carriage return with \r. 

        Unfortunately, this is the reason of why monkeypatching won't give the hoped results.
        """

        monkeypatch.setattr('builtins.input', lambda _: '\r')
        # with open("/dev/tty","w") as fp:
        #    fp.write('\r')

        menu = TerminalMenuSelector(mbo_test)
        selected_id = menu.select(real_artist)

        assert selected_id == madonna_id


def test_main(end_to_end_mokup,
              end_to_end_expected_result,
              mbo_test,
              lyricsOVH_test,
              end_to_end_artist):
    """end to end
    """
    menu = FirstElementMenuSelector(mbo_test)
    assert abs(main(end_to_end_artist, mbo_test, lyricsOVH_test, menu) -
               end_to_end_expected_result) < 0.0001


def test_main2(end_to_end_mokup_2,
               end_to_end_expected_result_2,
               mbo_test,
               lyricsOVH_test,
               end_to_end_artist):
    """end to end
    testing that an error in a song is excluded by the counting
    test also that having two similar artist doesn't affect the target result
    """
    menu = FirstElementMenuSelector(mbo_test)
    assert abs(main(end_to_end_artist, mbo_test, lyricsOVH_test, menu) -
               end_to_end_expected_result_2) < 0.0001


def test_count_words_in_text():
    test_string = "a v c"
    result = 3
    assert count_words_in_text(test_string) == result
