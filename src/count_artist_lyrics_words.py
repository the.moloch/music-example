import abc
import argparse
from typing import Dict, Generator, Tuple
import requests
import urllib
import time
from simple_term_menu import TerminalMenu
from simplejson.errors import JSONDecodeError


class LyricsNotFoundException(Exception):
    """ raised with the lyric is not found

    Args:
        Exception ([type]): [description]

    Returns:
        [type]: [description]
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class LyricsGenericException(Exception):
    """ raised for any other reason apart from being not found

    Args:
        Exception ([type]): [description]
    """

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class AbstractArtistLookup(abc.ABC):

    @abc.abstractmethod
    def author_generator(self,
                         author_name: str,
                         page_size: int = 100) -> Generator:
        raise NotImplementedError

    @abc.abstractmethod
    def get_first_author_id(self,
                            author_name: str,
                            offset: int = 0) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    def song_name_generator(self,
                            artist_id: str,
                            page_size: int = 100) -> Generator:
        raise NotImplementedError


class AbstractLyricsLookup(abc.ABC):

    @abc.abstractmethod
    def search_artist_lyric_by_title(self, artist: str, title: str) -> str:
        raise NotImplementedError


class MenuSelector(abc.ABC):

    @abc.abstractmethod
    def select(self, artist_name: str) -> str:
        raise NotImplementedError


class MusicBrainzOrg(AbstractArtistLookup):

    AUTHOR_QUERY_URL = "https://musicbrainz.org/ws/2/artist"
    AUTHOR_WORK_QUERY_URL = "https://musicbrainz.org/ws/2/work"

    def __init__(self, user_agent: str, wait_time: float = 1.1) -> None:
        """Wrapper for musicbrainz.org

        please use a meaningful user agent
        https://musicbrainz.org/doc/MusicBrainz_API/Rate_Limiting#Provide_meaningful_User-Agent_strings

        schema output is fixed to json
        Args:
            user_agent (str): the user agent header to add    
            wait_time (float): the cooling time between each request
        """
        self.user_agent = user_agent
        self.wait_time = wait_time
        self.schema_output = "json"

    def search_author(self,
                      author_name: str,
                      offset: int = 0,
                      page_size: int = 100
                      ) -> Dict:
        """[summary]

        Args:
            author_name (str): [description]
            offset (int, optional): [description]. Defaults to 0.
            page_size (int, optional): [description]. Defaults to 100.
        """
        # https://musicbrainz.org/ws/2/artist?query=madonna&limit=100&offset=0&fmt=json
        result = requests.get(MusicBrainzOrg.AUTHOR_QUERY_URL, {
            'query': author_name,
            'limit': page_size,
            'offset': offset,
            'fmt': self.schema_output
        }, headers={
            "User-Agent": self.user_agent
        })
        json_payload = result.json()

        return json_payload

    def author_generator(self, author_name: str, page_size: int = 100) -> Generator:
        """[summary]

        Args:
            author_name (str): [description]
            page_size (int, optional): [description]. Defaults to 100.

        Yields:
            Generator: [description]
        """
        offset = 0
        count = None
        while True:
            result = self.search_author(author_name=author_name,
                                        offset=offset,
                                        page_size=page_size)
            if count is None:
                count = result["count"]

            for author in result["artists"]:
                yield author

            offset = offset + len(result["artists"])

            if offset >= count:
                break

    def get_first_author_id(self,
                            author_name: str,
                            offset: int = 0) -> str:
        """[summary]

        Args:
            author_name (str): [description]
            offset (int, optional): [description]. Defaults to 0.

        Returns:
            str: [description]
        """
        artist = self.search_author(author_name, offset)
        artist_id = artist["artists"][0]["id"]
        return artist_id

    def fetch_work(self,
                   mbid_artist: str,
                   page_size: int = 100,
                   offset: int = 0) -> Dict:
        """Generator Fetches works of an artist
        From https://musicbrainz.org/doc/Work
        In MusicBrainz terminology, a work is a distinct intellectual or artistic 
        creation, which can be expressed in the form of one or more audio recordings. 
        While a work in MusicBrainz is usually musical in nature, it is not 
        necessarily so. For example, a work could be a novel, play, poem or essay, 
        later recorded as an oratory or audiobook.

        Args:
            mbid_artist (string): [description]
            offset (int, optional): [description]. Defaults to 0.
            page_size (int, optional): [description]. Defaults to 100.
        Returns:
            [type]: [description]
        """
        # https://musicbrainz.org/ws/2/work?artist=79239441-bfd5-4981-a70c-55c3f15c1287&limit=100&offset=0&fmt=json
        if self.wait_time > 0:
            time.sleep(self.wait_time)

        result = requests.get(MusicBrainzOrg.AUTHOR_WORK_QUERY_URL, {
            'artist': mbid_artist,
            'limit': page_size,
            'offset': offset,
            'fmt': self.schema_output
        }, headers={
            "User-Agent": self.user_agent
        })
        json_payload = result.json()
        return json_payload

    def song_name_generator(self,
                            artist_id: str,
                            page_size: int = 100) -> Generator:
        """ extract work name

        Args:
            artist_id ([type]): [description]
            page_size (int, optional): [description]. Defaults to 100.
            schema_output (str, optional): [description]. Defaults to "json".

        Yields:
            Generator: [description]
        """
        offset = 0
        while True:

            result = self.fetch_work(artist_id, page_size, offset=offset)

            if "work-count" not in result or len(result["works"]) == 0:
                break

            for work in result["works"]:
                if work["type"] == "Song":
                    yield work["title"]

            if result["work-count"] > offset:
                offset = offset+page_size
            else:
                break


class LyricsOVH(AbstractLyricsLookup):

    LOOKUP_ARTIST_LYRICS_FORMAT = 'https://api.lyrics.ovh/v1/{}/{}'

    def __init__(self):
        """Wraps lyrics.ovh api.
        """
        pass

    def parse_url(self, artist: str, title: str) -> str:
        """[summary]

        Args:
            artist (str): [description]
            title (str): [description]

        Returns:
            [type]: [description]
        """
        return LyricsOVH.LOOKUP_ARTIST_LYRICS_FORMAT.format(urllib.parse.quote(artist),
                                                            urllib.parse.quote(title))

    def search_artist_lyric_by_title(self, artist: str, title: str) -> str:
        """Search artist lyric by title

        Args:
            artist (str): [description]
            title (str): [description]
        """
        try:
            result = requests.get(self.parse_url(artist, title))
        except TypeError:
            raise LyricsGenericException(title)
        try:
            p = result.json()
            try:
                # NOTE important to skip the first line
                # which is the line of the title
                # separator is \r\n
                # print(type(p["lyrics"]))
                # we need only the content of the lyrics
                res = p["lyrics"].split("\r\n")
                # rejoin
                return "".join(res[1:])
            except KeyError:
                if 'error' in p:
                    raise LyricsNotFoundException(title)
                raise LyricsGenericException(title)
        except JSONDecodeError:
            raise LyricsGenericException(title)


def count_words_in_text(text: str) -> int:
    """[summary]

    Args:
        text (str): [description]

    Returns:
        int: [description]
    """

    return len(text.split())


class FirstElementMenuSelector(MenuSelector):

    def __init__(self, artist_manager: AbstractArtistLookup) -> None:
        """[summary]

        Args:
            artist_manager (AbstractArtistLookup): [description]
        """
        self.artist_manager = artist_manager

    def select(self, artist_name: str) -> str:
        return self.artist_manager.get_first_author_id(artist_name)


class TerminalMenuSelector(MenuSelector):

    def __init__(self, artist_manager: AbstractArtistLookup) -> None:
        """[summary]

        Args:
            mbo (AbstractArtistLookup): [description]
        """
        self.artist_manager = artist_manager

    def extract_features(self, artist_name: str) -> Tuple:
        """[summary]

        Args:
            artist_name (str): [description]

        Returns:
            Tuple: [description]
        """
        authors_id = {}
        menu = []

        i = 0
        for author in self.artist_manager.author_generator(artist_name):
            authors_id[i] = author["id"]
            item = ""
            if 'name' in author:
                item = f"{author['name']}"

            if 'type' in author:
                item += f" {author['type']}"

            if 'country' in author:
                item += f" {author['country']}"

            if 'disambiguation' in author:
                item += f" {author['disambiguation']}"

            menu.append(item)

            i = i+1

        return authors_id, menu

    def select(self, artist_name: str) -> str:
        """[summary]

        Args:
            artist_name (str): [description]

        Returns:
            str: [description]
        """
        authors_id, menu = self.extract_features(artist_name)
        terminal_menu = TerminalMenu(menu)
        selected_option = terminal_menu.show()
        return authors_id[selected_option]


def main(artist_name: str,
         artists_lookup_manager: AbstractArtistLookup,
         lyrics_lookup_manager: AbstractLyricsLookup,
         menu: MenuSelector,
         show_not_found: bool = False) -> float:
    """driver 
    average number of words of the lyrics by an author.

    Args:
        artist_name (str): name of the artist
        artist_lookup_manager (AbstractArtistLookup): lookup object
        lyrics_lookup_manager (AbstractLyricsLookup): lookup object
        menu (MenuSelector): menu selector 
        show_not_found (bool): show on console songs not found or not readable
    Returns:
        [float]: average number of words in the lyrics of an artist
    """
    average_number_of_words = 0

    '''
    Draft:
    I need to query muicbrainz.org, and get the artist, there are two cases:
    - first artist -> automatically select the first artist
    - select artist -> this case need to prompt a menu to select which one are we talking about.

    Once selected, I'll have the artist id, I can retrieve the all works:
    - works are of many type, I need to filter just the "Song" type and take the title.
    
    for each work I need to get the lyrics, count the words and store somewhere

    eventually average the total words.
    '''

    artist_id = menu.select(artist_name)

    if artist_id:
        total_words = 0
        song_number = 0.0
        for song in artists_lookup_manager.song_name_generator(artist_id):
            try:
                lyrics = lyrics_lookup_manager.search_artist_lyric_by_title(
                    artist=artist_name, title=song)
                # assert type(lyrics)==type(str)
                # count the words
                number = count_words_in_text(lyrics)
                #print(number)
                if number > 0:
                    total_words = total_words+number
                    song_number = song_number+1
                    print(song_number)
            except (LyricsNotFoundException, LyricsGenericException) as e:
                if show_not_found:
                    print(e)
        try:
            average_number_of_words = total_words/song_number
        except ZeroDivisionError:
            pass

    return average_number_of_words


if __name__ == "__main__":
    """CLI Driver
    """
    parser = argparse.ArgumentParser(
        description="Counts the average number of words in the lyrics of an artist,"
                    " the output is the list of lyrics not found ending with the average"
                    " words of the lyrics found")
    parser.add_argument(
        '-f', '--first', action='store_true',
        help='choose the first artist automatically')
    parser.add_argument(
        '-u', '--no-found', action='store_true',
        help='show on console songs not found or not readable'
    )
    parser.add_argument(
        "user_agent", type=str, nargs=1,
        help="the name of your user agent, please check: "
             "https://musicbrainz.org/doc/MusicBrainz_API/Rate_Limiting"
    )
    parser.add_argument('artist_name',
                        type=str, nargs='+',
                        help='the name of the artist')

    args = parser.parse_args()

    artist_name = " ".join(args.artist_name)

    mbo = MusicBrainzOrg("".join(args.user_agent))
    lyric_lookup = LyricsOVH()

    if args.first:
        menu = FirstElementMenuSelector(mbo)
    else:
        # here goes the piece for adding the command line interface
        menu = TerminalMenuSelector(mbo)

    print(main(artist_name,
               mbo,
               lyric_lookup,
               menu,
               args.no_found))
